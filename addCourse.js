let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e)=>{
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	let token = localStorage.getItem("token")
	let isAdmin = localStorage.getItem("isAdmin")
	/* Activity
		Create a fetch request to create a new course
		Make sure that you send the token with this request
		If the server responds with "true" for our request, redirect the user to the courses page
		If not, show an error message in an alert
	*/

	if(!token){
		alert("You need to login first to add a course")
		window.location.replace("./login.html")
	}else{
		if(isAdmin === false){
			alert("Adding a new course unsuccess. Please try again.")
			window.location.replace("./courses.html")

		}else{
			fetch('http://localhost:4000/courses/', {
				method:"POST",
				headers:{
					Authorization: `Bearer ${token}`,
					'Content-Type':'application/json'
				},
				body: JSON.stringify({
					name:courseName,
					description:description,
					price: price
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data){
					alert("New course added successfully.")
					window.location.replace("./courses.html")
				}else{
					alert("Adding a new course unsuccess. Please try again.")
					window.location.replace("./courses.html")
				}
			})
		}
	}
})