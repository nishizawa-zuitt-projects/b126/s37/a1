fetch('http://localhost:4000/courses')
// The line above specifies the URL of the server we want to make a request to 

.then(res=> res.json())
/*
The line above receives the response(res) from the server and converts it to JSON,
which is then immediately converted to a JavaScript Object
*/
.then(data => {
/*
data here refers to the converted data from line 59, which is now ready to be used
*/
	// console.log(data[0].name) // show the data in our console

	/* Activity
		Change "Zuitter Booking Services" to the name of any of your courses
		Answer goes insdie these two curly braces

		Once answered, please copy the entire fetch request to an index.js file
		inside of s37/a1 (create the folder if you need to)
	*/

	docHeader.innerText = data[0].name
})